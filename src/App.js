import React, { Component } from "react";
import Map from "./components/map";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Map isMarkerShown />
      </React.Fragment>
    );
  }
}

export default App;
