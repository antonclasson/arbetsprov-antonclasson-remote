import React, { Component } from "react";

class TableBody extends Component {
  render() {
    return (
      <tbody className="table-body">
        {this.props.markers.length > 0 &&
          this.props.markers.map(m => (
            <tr key={m._id}>
              <td
                className="table-cell"
                onClick={() => this.props.onFocus(m._id)}
              >
                {// Add a defualt name "Marker (id)"" if
                //  no name has been given through the popup
                m.tag === "" || m.tag === undefined ? "Marker " + m._id : m.tag}
              </td>
              <td className="table-cell">
                <button
                  className="btn btn-danger"
                  onClick={() => this.props.onDelete(m._id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
      </tbody>
    );
  }
}

export default TableBody;
