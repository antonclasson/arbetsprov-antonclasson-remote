import React, { Component } from "react";
import GoogleMapReact from "../google-map/google-map-react";
import ControlledPopup from "./controlledPopup";
import config from "../config";
import Marker from "./marker";
import List from "./list";
import ListButton from "./listbutton";

class Map extends Component {
  // Initialize state with values
  state = {
    center: { lat: 0, lng: 0 },
    zoom: 10,
    markers: [],
    totalCount: 0,
    newMarker: false,
    showList: false
  };

  oldCenter = {};

  // Get values from config file and set the state to these
  getValues = () => {
    this.setState({
      center: config.center,
      zoom: config.zoom
    });
  };

  // Read the values when the component is mounted
  componentDidMount() {
    this.getValues();
  }

  // Set the Map options in accordance witht the APIs documentation
  createMapOptions = () => {
    return {
      disableDoubleClickZoom: true
    };
  };

  onClick = ({ x, y, lat, lng, event }) => {
    const { markers, totalCount } = this.state;
    let marker = { _id: totalCount, lat: lat, lng: lng, tag: "" };
    markers.push(marker);
    this.setState({ markers, totalCount: totalCount + 1, newMarker: true });
  };

  // After the popup is closed, reset the newMarker
  // to false in order to allow for it to open again
  handleCloseModal = () => {
    this.setState({ newMarker: false });
  };

  toggleShowList = () => {
    this.setState({ showList: !this.state.showList });
  };

  // Remove marker with matching id by filtering it from state.markers
  handleDelete = id => {
    const markers = this.state.markers.filter(m => m._id !== id);
    this.setState({ markers });
  };

  // Cet the center in order to focus on the marker with matching id
  handleFocus = id => {
    const markerWithId = this.state.markers.filter(m => m._id === id);
    const { lat, lng } = markerWithId[0];
    let { center } = this.state;
    center.lat = lat;
    center.lng = lng;
    this.setState({ center });
  };

  // Compare center to oldCenter:
  // If there is a difference,
  // this means that a marker has been chosen to focus on with handleFocus
  // return true and set oldCenter to center.
  // Otherwise return false
  checkIfCenterIsNew = () => {
    const { center } = this.state;
    if (
      center.lat !== this.oldCenter.lat ||
      center.lng !== this.oldCenter.lng
    ) {
      this.oldCenter = { ...center };
      return true;
    }
    return false;
  };

  render() {
    const { center, zoom, markers, showList } = this.state;

    const newCenter = this.checkIfCenterIsNew();

    return (
      <div style={{ height: "100vh", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: config.apiKey }}
          // Pass the center prop if only a new center has been focused on by a marker
          center={newCenter ? center : undefined}
          zoom={zoom}
          options={this.createMapOptions}
          onClick={this.onClick}
        >
          {markers.length > 0 &&
            markers.map(m => (
              <Marker
                key={m._id}
                id={m._id}
                lat={m.lat}
                lng={m.lng}
                text={m._id}
              />
            ))}
        </GoogleMapReact>
        {this.state.newMarker && (
          <ControlledPopup
            newMarker={markers[markers.length - 1]}
            handleCloseModal={this.handleCloseModal}
          />
        )}
        {showList ? (
          <List
            markers={markers}
            onFocus={this.handleFocus}
            onDelete={this.handleDelete}
            toggle={showList}
            toggleButton={this.toggleShowList}
          />
        ) : (
          <ListButton toggle={showList} onClick={this.toggleShowList} />
        )}
      </div>
    );
  }
}

export default Map;
