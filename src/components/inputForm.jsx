import React, { Component } from "react";

class InputForm extends Component {
  state = { data: "" };

  handleChange = event => {
    this.setState({ data: event.target.value });
  };

  render() {
    const { data } = this.state;

    return (
      <form onSubmit={() => this.props.handleSubmit(data)}>
        <input
          autoFocus
          type="text"
          value={data}
          onChange={this.handleChange}
        />
      </form>
    );
  }
}

export default InputForm;
