import React, { Component } from "react";
import ListButton from "./listbutton";
import TableBody from "./tableBody";
import SearhBar from "./seachBar";

class List extends Component {
  state = {
    searchQuery: ""
  };

  // Save the search query to the state
  handleSearch = query => {
    this.setState({ searchQuery: query });
  };

  // Filter the markers based on the search query
  filterMarkers = () => {
    const { searchQuery } = this.state;
    const { markers } = this.props;
    let filtered = markers;
    if (searchQuery) {
      filtered = markers.filter(m => {
        // Check that the marker is not undefined, nor its tag
        if (!m || !m.tag) {
          return false;
        }
        return m.tag.toLowerCase().includes(searchQuery.toLowerCase());
      });
    }
    return filtered;
  };

  render() {
    const filtered = this.filterMarkers();

    return (
      <div className="table-container">
        <SearhBar value={this.state.searchQuery} onChange={this.handleSearch} />
        <table>
          <TableBody
            markers={filtered}
            onFocus={this.props.onFocus}
            onDelete={this.props.onDelete}
          />
        </table>
        <ListButton
          toggle={this.props.toggle}
          onClick={this.props.toggleButton}
        />
      </div>
    );
  }
}

export default List;
