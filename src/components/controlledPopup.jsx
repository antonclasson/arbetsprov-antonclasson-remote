import React from "react";
import Popup from "reactjs-popup";

class ControlledPopup extends React.Component {
  state = { data: "Name" };

  constructor(props) {
    super(props);
    this.state = { open: true };
  }
  openModal = () => {
    this.setState({ open: true });
  };

  // Adds a defualt name to the marker
  addDefaultNameToMarker = () => {
    const { newMarker } = this.props;
    newMarker.tag = "Marker " + newMarker._id;
  };

  closeModal = () => {
    if (!this.state.open) return;

    // Check that a tag has been given
    const { tag } = this.props.newMarker;
    if (!tag || tag === "") {
      this.addDefaultNameToMarker();
    }

    this.props.handleCloseModal();
    this.setState({ open: false });
  };

  handleChange = event => {
    this.setState({ data: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { data } = this.state;
    if (!data || data === "") {
      this.addDefaultNameToMarker();
    } else {
      this.props.newMarker.tag = data;
    }
    this.closeModal();
  };

  style = {
    background: "#F8F9F8",
    border: "5px solid #3A3E58",
    borderRadius: "5px",
    color: "#1A1C1D",
    maxWidth: "250px"
  };

  render() {
    return (
      <div>
        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
          contentStyle={this.style}
        >
          <div>
            <div className="header">Add a name to the marker</div>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input
                  className="center-block form-control"
                  autoFocus
                  type="text"
                  value={this.state.data ? this.state.data : ""}
                  onChange={this.handleChange}
                />
              </div>
            </form>
            <button
              className="btn btn-success center-block"
              onClick={this.handleSubmit}
            >
              Submit
            </button>
            {/* <button className="btn btn-danger" onClick={this.closeModal}>
              Close
            </button> */}
          </div>
        </Popup>
      </div>
    );
  }
}

export default ControlledPopup;
