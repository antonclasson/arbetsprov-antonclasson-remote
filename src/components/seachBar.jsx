import React, { Component } from "react";

class SearhBar extends Component {
  render() {
    const { value } = this.props;

    return (
      // <thead className="table-header">
      <input
        type="text"
        name="query"
        className="form-control seach-bar"
        placeholder="Search..."
        value={value}
        onChange={e => this.props.onChange(e.currentTarget.value)}
      />
      // </thead>
    );
  }
}

export default SearhBar;
