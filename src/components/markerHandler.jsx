import React, { Component } from "react";
import Marker from "./marker";

class MarkerHandler extends Component {
  state = { markers: [] };

  componentDidMount() {
    this.props.onRef(this);
  }

  handleClick = ({ x, y, lat, lng, event }) => {
    // Create a marker at location(lat, lng) and store it in a list
    const { markers } = this.state;
    const marker = { _id: markers.length, lat: lat, lng: lng };
    markers.push(marker);
    console.log(markers);
    this.setState({ markers });
  };

  render() {
    const { markers } = this.state;

    console.log(markers);

    return (
      <div>
        {markers.length > 0 &&
          markers.map(m => (
            <Marker key={m._id} lat={m.lat} lng={m.lng} text={m._id} />
          ))}
      </div>
    );
  }
}

export default MarkerHandler;
