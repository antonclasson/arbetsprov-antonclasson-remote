import React, { Component } from "react";

class ListButton extends Component {
  render() {
    const { toggle } = this.props;

    let classes = "btn btn-primary btn-lg";
    // Apply classes based on in the button will
    // hide the list or show the list in order
    // to keep it placed at the desired location
    !toggle
      ? (classes += " show-list")
      : (classes += " center-block hide-list");

    return (
      <button className={classes} onClick={this.props.onClick}>
        {toggle ? "Hide List" : "Show List"}
      </button>
    );
  }
}

export default ListButton;
