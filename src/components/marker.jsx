import React from "react";

// Small funtion to render the individual markers
// with the style set in markerStyle and with a
// text, which in this case is a number indicating
// the markers ID

const Marker = ({ id, text }) => {
  return <button className="marker">{text}</button>;
};

export default Marker;
